print('done')
import pytest

import library
#from library import calculate_rectangle_square


testcases_calculate_rectangle_square_positive = [
    (5, 5, 25.0),
    (5, 5.5, 27.5),
    (1, 1, 1),
]

testcases_calculate_rectangle_square_negative = [
    (-5, 5, ValueError),
    (5, -5.5, ValueError),
    (0, 1, ValueError),
    (1, 0, ValueError),
]




@pytest.mark.parametrize('side_one, side_two, expected', testcases_calculate_rectangle_square_positive)
def test_calculate_rectangle_square_positive(side_one, side_two, expected):
    assert library.calculate_rectangle_square(side_one, side_two) == expected, 'something went wrong'


@pytest.mark.parametrize('side_one, side_two, expected_error', testcases_calculate_rectangle_square_negative)
def test_calculate_rectangle_square_negative(side_one, side_two, expected_error):
    with pytest.raises(expected_error):
        library.calculate_rectangle_square(side_one, side_two)

def calculate_rectangle_square(side_one: int | float, side_two: int | float) -> int | float:
    """if any side is zero or less - raise ValueError (according to doc #6556656)"""
    rectangle_square = side_one * side_two
    return rectangle_square


# @pytest.mark.parametrize('side_one', [1, 0])
# @pytest.mark.parametrize('side_two, expected', [(1, 1), (1, 0)])
# def test_calculate_rectangle_square_positive(side_one, side_two, expected):
#     assert calculate_rectangle_square(side_one, side_two) == expected

