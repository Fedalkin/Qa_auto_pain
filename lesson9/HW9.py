# Написати функцію, котра отримує стрічку та повертає цю стрічку, проте очищену від малих літер
# ("длоОЛО  55 ! " має повернути "ОЛО"). окрім того довжина стрічки не має перевищувати 25 символів
# (все що більше просто обрізається)
# покрити тестами
# перевірити flake8, mypy
# не забувайте про аннотації типів і докстрінги при потребі
import textwrap

def text_cutting_foo1(input_text):
    if type(input_text) not in [str]:
        raise TypeError
    text_list = list(input_text)
    upper_list = []
    for symbol in text_list:
        if symbol.isalpha() and symbol.isupper():
            upper_list += symbol

    output_text = ''.join(upper_list)[:25]
    return output_text

res = text_cutting_foo1 ('For 55 strings StringIO can be Used Fike a File opEned in a Text moDe, and '
                        'for Bytes a BytesIO cAn be Used Like a file Opened in a biNary Mode.')

print(res)


def text_cutting_foo2(input_text):
    if isinstance(input_text, str):
        output_text = ''
        for symbol in input_text:
            if symbol.isalpha() and symbol.isupper():
                output_text += symbol
    else:
        raise TypeError
    return textwrap.shorten(output_text, width=25, placeholder='...')


res = text_cutting_foo2('For 55 strings StringIO can be Used Fike a File opEned in a Text moDe, and for Bytes '
                       'a BytesIO cAn be Used Like a file Opened in a biNary Mode.')

print(res)
