import functools
from typing import Callable

ERROR_MESSAGE = 'Bam'


def validator_more_zero(func: Callable):
    """allows only positive arguments"""

    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        arguments = [*args, *kwargs.values()]
        for arg in arguments:
            if type(arg) not in [int, float]:
                continue
            else:
                if arg <= 0:
                    raise ValueError(ERROR_MESSAGE)

        result = func(*args, **kwargs)

        return result

    return wrapper


@validator_more_zero
def calculate_rectangle_square(side_one: int | float, side_two: int | float) -> int | float:
    """if any side is zero or less - raise ValueError (according to doc #6556656)"""
    rectangle_square = side_one * side_two
    return rectangle_square


@validator_more_zero
def calculate_rectangle_perimeter(side_one: int | float, /,  side_two: int | float, *, force_return_float=False) -> int | float:
    """if any side is zero or less - raise ValueError (according to doc #6556699)"""
    rectangle_perimeter = (side_one + side_two) * 2

    if force_return_float:
        rectangle_perimeter = rectangle_perimeter * 1.0
    return rectangle_perimeter


@validator_more_zero
def calculate_volume_sphere(radius: int | float) -> float:
    """if any side is zero or less - raise ValueError (according to doc #6556449)"""
    volume_sphere = (4 / 3) * 3.1416 * (radius ** 3)
    return round(volume_sphere, 2)
