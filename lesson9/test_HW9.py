import pytest
import textwrap


@pytest.mark.parametrize('output_text, expected_len', [('FSIOUFFETDBBIOAULONM', 25)])
def test_text_cutting_foo2_len_less25(output_text, expected_len):
    assert len(text_cutting_foo2(output_text)) <= expected_len


@pytest.mark.parametrize('output_text, expected_upper', [('FSIOUFFETDBBIOAULONM', True)])
def test_text_cutting_foo2_isupper(output_text, expected_upper):
    assert text_cutting_foo2(output_text).isupper() == expected_upper


@pytest.mark.parametrize('output_text, expected_alpha', [('FSIOUFFETDBBIOAULONM', True)])
def test_text_cutting_foo2_isalpha(output_text, expected_alpha):
    assert text_cutting_foo2(output_text).isalpha() == expected_alpha


@pytest.mark.parametrize('output_text, expected_str', [('FSIOUFFETDBBIOAULONM', str)])
def test_text_cutting_foo2_str(output_text, expected_str):
    assert type(text_cutting_foo2(output_text)) == expected_str


def text_cutting_foo2(input_text):
    if isinstance(input_text, str):
        output_text = ''
        for symbol in input_text:
            if symbol.isalpha() and symbol.isupper():
                output_text += symbol
    else:
        raise TypeError
    return textwrap.shorten(output_text, width=25, placeholder='...')


text_cutting_foo2('For 55 strings StringIO can be Used Fike a File opEned in a Text moDe, and for Bytes '
                       'a BytesIO cAn be Used Like a file Opened in a biNary Mode.')
