# 1. Напишіть функцію, яка визначає сезон за датою. Функція отримує стрінг у форматі
# "[день].[місяць]"  (наприклад "12.01", "30.08", "1.11" і тд) і повинна повернути стрінг
# з відповідним сезоном, до якого відноситься ця дата ("літо", "осінь", "зима", "весна")


# user_date = input('Enter a date if format "dd.mm": ')
# def date_function(user_date):
#
#     if user_date.endswith('01') or user_date.endswith('02') or user_date.endswith('12'):
#         return 'Winter'
#     elif user_date.endswith('03') or user_date.endswith('04') or user_date.endswith('05'):
#         return 'Spring'
#     elif user_date.endswith('06') or user_date.endswith('07') or user_date.endswith('08'):
#         return 'Summer'
#     elif user_date.endswith('09') or user_date.endswith('10') or user_date.endswith('11'):
#         return 'Autumn'
#     else:
#         return None
# #
# #
# season = date_function(input('Enter a date if format "dd.mm": '))
# print(season)


# def date_function(user_date):
#     month = user_date.split('.')[-1]
#
#     if month in ('01', '02', '12'):
#         return 'Winter'
#     elif month in ('03', '04', '05'):
#         return 'Spring'
#     elif month in ('06', '07', '08'):
#         return 'Summer'
#     elif month in ('09', '10', '11'):
#         return 'Autumn'
#     else:
#         return None

# 2. Напишіть функцію "Тупий калькулятор", яка приймає два числових аргументи і строковий,
# який відповідає за операцію між ними (+ - / *). Функція повинна повертати значення
# відповідної операції (додавання, віднімання, ділення, множення), інші операції не допускаються.
# Якщо функція оримала невірний тип данних для операції (не числа) або неприпустимий (невідомий)
# тип операції вона повинна повернути None і вивести повідомлення "Невірний тип даних"
# або "Операція не підтримується" відповідно.


# def stupid_calc(number1: float, operator: str, number2: float):
#     result = None
#     if not isinstance(number1, (int, float)) or not isinstance(number2, (int, float)):
#         print("Невірний тип даних")
#         return result
#     if operator == '+':
#         result = number1 + number2
#     elif operator == '-':
#         result = number1 - number2
#     elif operator == '/':
#         result = number1 / number2
#     elif operator == '*':
#         result = number1 * number2
#     else:
#         print("Операція не підтримується")
#
#     print(result)
#     return result
#
# # number1 = float(input('Enter first number: '))
# # operator = input('Enter arithmetic operator (+, -, /, *): ')
# # number2 = float(input('Enter second number: '))
#
# # stupid_calc(number1, operator, number2)
# stupid_calc(5, '+', 5)

#===================================================

# def add(first, second):
#     return first + second
#
#
# def stupid_calc(number1: float, operator: str, number2: float):
#     calc_dict = {
#         '+': add,
#         '-': lambda a, b: a - b,
#         '*': lambda a, b: a * b,
#         '/': lambda a, b: a / b,
#     }
#     func = calc_dict.get(operator)
#     if not func:
#         print('Wrong operation')
#         return
#     try:
#         return func(number1, number2)
#     except Exception:
#         print('Wrong data types')
#         return
#
#
# res = stupid_calc('1', '+',2)
# print(res)
#
# # number1 = float(input('Enter first number: '))
# # operator = input('Enter arithmetic operator (+, -, /, *): ')
# # number2 = float(input('Enter second number: '))
#
# # stupid_calc(number1, operator, number2)
# stupid_calc(5, '+', 5)



# 3. Напишіть докстрінг для обох функцій