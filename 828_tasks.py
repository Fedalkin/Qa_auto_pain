#todo 102. Напишіть програму, в якій користувач вводить пароль і якщо він співпадає із наперед визначеним паролем для цього користувача,
# то виводиться повідомлення Password accepted.. У іншому випадку повідомлення буде Sorry, that is the wrong password..
# Вхідні дані:
#
# starlink
# 12345
# Вихідні дані:
#
# Password accepted.
# Sorry, that is the wrong password.
#
# user_input = input('Enter password: ')
# user_pass = 'starlink'
# if user_input == user_pass:
#     print('Password accetped.')
# else:
#     print('Sorry, that is the wrong password.')
#
#todo 103. Користувачем вводиться два імені. Використовуючи конструкцію розгалуження програма повинна вивести імена в алфавітному порядку.
#
# Вхідні дані:
#
# Guido van Rossum
# Dennis Ritchie
# Вихідні дані:
#
# Dennis Ritchie
# Guido van Rossum
#
# usr_input1 = input('Enter name: ')
# usr_input2 = input('Enter next name: ')
# usr_list = []
# usr_list.append(usr_input1)
# usr_list.append(usr_input2)
# usr_list.sort()
# print(usr_list[0])
# print(usr_list[1])
#-----------------------------------
# usr_input1 = input('Enter name: ')
# usr_input2 = input('Enter next name: ')
# if usr_input1.lower() < usr_input2.lower():
#     print(usr_input1)
#     print(usr_input2)
# else:
#     print(usr_input2)
#     print(usr_input1)
#-----------------------------------
# usr_input1 = input('Enter name: ')
# usr_input2 = input('Enter next name: ')
# if usr_input1.lower() > usr_input2.lower():
#     usr_input1, usr_input2 = usr_input2, usr_input1
#
# print(usr_input1)
# print(usr_input2)

#todo 104. Напишіть програму, яка виводить назви введених чисел. Користувач вводить ціле число. Якщо це число або 1 або 2 або 3,
# то виводиться повідомлення - назва числа, відповідно, One, Two, Three. В усіх інших випадках виводиться слово Unknown.
#
# Вхідні дані:
#
# 20
# 1
# 2
# 3
# Вихідні дані:
#
# Unknown
# One
# Two
# Three

# usr_input = input('Enter Num: ')
#
# if usr_input == '1':
#     print('One')
# elif usr_input == '2':
#     print('Two')
# elif usr_input == '3':
#     print('Three')
# else:
#     print('Unknown')

#todo 105. Користувач вводить дві різних англійські літери в окремих рядках. Напишіть програму,
# яка виводить повідомлення про місце розташування однієї літери відносно іншої у алфавіті.
#
# Вхідні дані:
#
# z
# a
# Вихідні дані:
#
# z is not less than a
# while True:
# first = input('letter1: ')
# second = input('letter2: ')
# if first < second:
#     print(f'{first} is less than {second}')
# else:
#     print(f'{first} is not less than {second}')

#todo 106. Напишіть програму, в якій користувач вводить значення температури, і, якщо це значення менше або дорівнює 0 градусів Цельсія,
# необхідно вивести повідомлення A cold, isn’t it?. Якщо ж температура становить більше 0 і менше 10 градусів Цельсія повідомлення буде Cool.,
# у інших випадках Nice weather we’re having..
#
# Вхідні дані:
#
# 12.5
# -5
# 9
# Вихідні дані:
#
# Nice weather we're having.
# A cold, isn't it?
# Cool.

