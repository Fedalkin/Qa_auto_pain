# from pprint import pprint
# import requests
#
# url = 'https://dummyjson.com/products?limit=100'
#
# response = requests.get(url)
# # print(response)
#
# our_products = response.json()
# products = our_products.get('products', [])
#
# total_cost = 0
# total_apple_cost = 0
#
# # for product in products:
# #     # pprint(product)
# #     product_price = product.get('price', 0)
# #     product_stock = product.get('stock', 0)
# #     total_cost += product_price * product_stock
# #
# # print(total_cost)
# # print('apple: ', total_apple_cost)
# # #=============================
# for product in products:
#     # pprint(product)
#     product_price = product.get('price', 0)
#     product_stock = product.get('stock', 0)
#     product_cost = product_price * product_stock
#     total_cost += product_cost
#
#     if product.get('brand').lower == 'apple':
#         total_apple_cost += product_cost
#
# print(total_cost)
# print('apple: ', total_apple_cost)

##+++++++++++++++++++++++++++++
# for product in products:
#     pictureurl = product['images']
#
#     picture_data = requests.get(pictureurl[0])
#     with open(f'./Images/{product.get("id", "")}-{product.get("brand", "")}.jpg', 'bw') as file:
#         file.write(picture_data.content)
#
#     product_price = product.get('price', 0)
#     product_stock = product.get('stock', 0)
#     product_cost = product_price * product_stock
#     total_cost += product_cost
#
#     if product.get('brand').lower == 'apple':
#         total_apple_cost += product_cost
#
# print(total_cost)
# print('apple: ', total_apple_cost)

#===================================================
from pprint import pprint
import requests

# url = 'https://dummyjson.com/products/{page}'
#
# total_cost = 0
# total_apple_cost = 0

# for product in products:
#     # pprint(product)
#     product_price = product.get('price', 0)
#     product_stock = product.get('stock', 0)
#     total_cost += product_price * product_stock
#
# print(total_cost)
# print('apple: ', total_apple_cost)
#=============================
# for page in range(1, 101):
#     product = requests.get(url.format(page=page)).json()
#     print(product)
#     product_price = product.get('price', 0)
#     product_stock = product.get('stock', 0)
#     product_cost = product_price * product_stock
#
#     if product.get('brand').lower() == 'apple':
#         total_apple_cost += product_cost
#
#     total_cost += product_cost
# print(total_cost)
# print('apple: ', total_apple_cost)
#===============================================
"""
string = 'aaaaaaaaaaaaaaaa' - True
string = 'bbbbbbbbbbbbbb' - True
string = 'aaaaaaabbbbbbbbbbbb' - True
string = 'aaaaabaaabbbbbbbbb' - True

"""
string1 = 'aaaaaaaaaaaaaaaa' # True
string2 = 'bbbbbbbbbbbbbb' # True
string3 = 'aaaaaaabbbbbbbbbbbb' # True
string4 = 'aaaaabaaabbbbbbbbb' # True

test = string4

# set
if len(set(test)) == 1:
    print(True)
else:
    string_length = len(test)
    string_length_maybe_only_a = test.rstrip('b')
    set_a = set(string_length_maybe_only_a)
    if len(set_a) == 1:
        print(True)
    else:
        print(False)