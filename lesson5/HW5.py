# 1_ є url https://dummyjson.com/users  (100 сторінок)
# вивести в консоль середній вік чоловіків з Brown волоссям, а також сформувати список людей,
# що проживають в місті Louisville
# обовязково прикріпити requirements.txt
import requests

url = 'https://dummyjson.com/users?limit=100'
response = requests.get(url)
print(response)

all_users = response.json()
users = all_users.get('users', [])

average_brhair_age = 0
louisville_cityzen = []
count = 0

for user in users:
    hair_color = user.get('hair', 0)
    user_age = user.get('age', 0)
    # if user['hair']['color'] == 'Brown':
    #     average_brhair_age += user_age
    hair = user.get('hair', None)

    if hair.get('color', None) == 'Brown':
        average_brhair_age += user_age
        count += 1
    adress = user.get('address')
    if adress.get('city') == 'Louisville':
        louisville_cityzen.append(f'{user["firstName"]} {user["lastName"]}')
        # print(louisville_cityzen)
avr_age = round(average_brhair_age / count, 2)



#         total_apple_cost += product_cost

print(avr_age)
print('From Louisville: ', str(louisville_cityzen).strip('[]'))




# 2_ створити АРІ на базі гугл таблиці, містить поля "назва товару", "опис товару", "ціна" (інтова чи флоат),
# "залишок" (інтовий чи флоат), "містить глютен" (булеве тру чи фолс (виставляєте прапорцем).
# заповнити мінімум 10 позицій. дані мають бути отримати по зовнішньому ключу "goods" (not "data")
# за допомогою requests завантажити створені дані. порахувати вартість всіх товарів та товарів без глютена.
# ну і requirements.txt прикріпіть