# 1. Напишіть цикл, який буде вимагати від користувача ввести слово,
# в якому є буква "о" (враховуються як великі так і маленькі).
# Цикл не повинен завершитися, якщо користувач ввів слово без букви "о".

# usr_input = (input('Text the word with \'o\' letter: '))
# while True:
#     usr_input = input('Text the word with \'o\' letter: ')
#     if 'o' in usr_input.lower():
#         print(usr_input)
#         break
#     print('Need O-word')

# 2. Є list з даними lst1 = ['1', '2', 3, True, 'False', 5, '6', 7, 8, 'Python', 9, 0, 'Lorem Ipsum'].
# Напишіть код, який свормує новий list (наприклад lst2), який містить лише змінні типу стрінг,
# які присутні в lst1. Зауважте, що lst1 не є статичним і може формуватися динамічно від запуску до запуску.
#
# list1 = ['1', '2', 3, True, 'False', 5, '6', 7, 8, 'Python', 9, 0, 'Lorem Ipsum']
# list2 = []
# for str_type in list1:
#     if isinstance(str_type, str):
#         list2.append(str_type)
# print(list2)

# 3. Є стрінг з певним текстом (можна скористатися input або константою).
# Напишіть код, який визначить кількість слів в цьому тексті,
# які закінчуються на "о" (враховуються як великі так і маленькі).

string0 = 'Where inO other programming languageso the indentation in code is for readability only, the indentationo in Python is very importanto'
word_list = string0.split() #split роздыляє стрінг і повертає в виді ліст
countO = []
for word_o in word_list:
    if word_o.lower().endswith('o'):
        countO.append(word_o)

print(countO)
print('O-Words in string -', len(countO))

lst = [1, 2, 3, 4, 5]

for i in lst:
    print(i)



# todo я зайобся пробувати, щоб написати красивіше. не працювало. запрацювало тільки так. знаю, що тут можна написати краще, але неїбу як.