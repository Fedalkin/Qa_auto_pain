# first = 5
# second = 0
#
# while True:
#
#     if first < second:
#         break
#     second += 1
#
#     if second % 2 == 0:
#         tmp_value = 0
#
#         while True:
#             print('inner loop', tmp_value)
#             tmp_value += 1
#             if tmp_value >= second:
#                 break
#
#         continue
#
#     print('Inside while loop')
#     print(f'{first} {second}')
#
# print('Outside loop')
#
# while True:
#     user_input = input('enter num(int): ')
#
#     if user_input.isdigit():
#         break
#     print('Wrong! give me int!')

# list

# my_list = [1, 2.0, True, 'four', None]
#
# print(type(my_list))
#
# print(my_list)
#
# my_list.append(6)
# print(my_list)
#
# my_list.remove(1)
#
# # true false
# print(1 in my_list)
#
# my_list += [1, 2, 3]
#
# my_list *= 2
# print(my_list)
#
# my_list = [0, 1.2, '2', True, 4, 5, 6]
# print(my_list)
#
# print(my_list[0])
# print(type(my_list[0]))
# print(type(my_list[1]))
# print(type(my_list[2]))
#
# print(my_list[2].endswith('4'))
#
# print(my_list[0:3])
#
# my_list = [0, 1.2, '2', True, [1, 2, 3]]
#
# print(my_list[-1])
# my_list[-1].append(4)
# print(my_list)
#
# my_str = '12345678'
# print(my_str[0])
#
# my_list[0] = [1, 2, 3]
# print(my_list)
#
# my_list_a = [1, 2, 3]
# my_list_b = [1, 2, 3]
#
# print(my_list_a == my_list_b)
#
# my_list = [0, 1.2, '2', True, [1, 2, 3]]
# my_list.remove(0)
# print(my_list)
#
# my_list.pop(2)
# print(my_list)
#
# my_list = []
# my_list.extend([1, 2, 3, 4])  # my_list += ([1, 2, 3, 4])
#
# print(bool(my_list))
#
# my_str = str ([1, 2, 3, 4])
#
# print(len(my_str))
#
# my_list = list('123456789')
# print(my_list)

# my_list = [0, 1.2, '2', True, [1, 2, 3]]
# print(my_list[-1][0])

# my_list = [0, 1.2, '2', True, [1, 2, 3]]
# list_len = len(my_list)
# idx = 0
# while idx < list_len:
#     print(my_list[idx])
#
#     idx += 1

# for loop

# my_list = [0, 1.2, '2', True, [1, 2, 3]]
#
# for i in my_list:
#     print('For loop', i)
#     print(type(i))
#
    # my_list.append(1) # зробили безкінечним, додаючи 1 елемент кожний прохід


my_list = [0, 1.2, '2', True, [1, 2, 3]]

for list_element in my_list: #str
    if isinstance(list_element, str):
        continue
    if isinstance(list_element, list):
        for inner_list_element in list_element:
            print('---->', inner_list_element)
    print('For loop', list_element)
    print(type(list_element))

    if isinstance(list_element, bool):
        break
# [::3] слайс по  кожному 3 елементу листа

# try except обробка помилок
# age = None
# try:
#     age = int(input('Enter num: '))
# except:
#     print('Wrong!')
#
# print(age)

# except ZeroDivisionError:
# except ValueError:

# try:
#     res = 10 / int(input('Enter the divisor (int): ')[0])
# except (ZeroDivisionError, ValueError) as err:
#     print('exception - should not be zero!')
#     print(type(err))
#     print(err.args)
# except ValueError:
#     print('exception - number please!')
# except:
#     print('Exception -----')

# try:
#     res = 10 / int(input('Enter the divisor (int): '))
# except (ZeroDivisionError, ValueError) as err:
#     print('exception - should not be zero!')
# else:
#     print('result is:', res)
# finally:
#     print('exception or not')

# tuple не редагуємий ліст

# my_tuple = (0, 1.2, '2', True, 'F', None [1,(1, 2, 3), 3], )
# print(my_tuple)
# print(type(my_tuple))
#
# print(my_tuple[4])
# print(my_tuple[1:4])
#
# for tuple_element in my_tuple:
#     print(tuple_element)

# my_tuple[0] = 10
# my_tuple.append(1)
# my_tuple.pop()

# my_tuple += (1, 2)
#
# print(my_tuple)
#
# my_tuple *= 2
# print(my_tuple)