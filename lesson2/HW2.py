#1. Сформуйте стрінг, в якому міститься інформація про певне слово. "Word [тут слово] has [тут довжина слова, отримайте з самого сдова]
# letters", наприклад "Word 'Python' has 6 letters". Для отримання слова для аналізу скористайтеся константою або функцією input().
print('TASK1 \n----------------------------------------------')
user_input = input('Input some word here: ')
wordlen = len(user_input)
# todo 4 варіанти на вибір
my_fstr = f"Word '{user_input}' has {wordlen} letters"
# my_fstr = "Word '{}' has {} letters".format(user_input, wordlen)
# my_fstr = "Word '%s' has %s letters" % (user_input, wordlen)
# my_fstr = 'Word ' + '\'' + user_input + '\' ' + 'has ' + str(wordlen) + ' letters, bitch!'
print(my_fstr)
print('\n(end)-----------------------------------------')
#
#2. Напишіть программу "Касир в кінотеатрі", яка попросіть користувача ввести свій вік (можно використати константу
# або функцію input(), на екран має бути виведено лише одне повідомлення, також подумайте над варіантами, коли введені невірні дані).
#   a. якщо користувачу менше 7 - вивести повідомлення"Де твої батьки?"
#   b. якщо користувачу менше 16 - вивести повідомлення "Це фільм для дорослих!"
#   c. якщо користувачу більше 65 - вивести повідомлення "Покажіть пенсійне посвідчення!"
#   d. якщо вік користувача містить цифру 7 - вивести повідомлення "Вам сьогодні пощастить!"
#   e. у будь-якому іншому випадку - вивести повідомлення "А білетів вже немає!"
# print('TASK2 \n----------------------------------------------')
# usr_age = input('Для придбання квитків вкажіть свій вік цифрами: ')
# MAX_AGE_LIMIT = 90
# MIN_AGE_LIMIT = 0
# if not usr_age.isdigit():
#     print('Сказав же, вік вводити цифрами! Йди звідси!')
# else:
#     usr_int = int(usr_age)
#     if usr_int > MAX_AGE_LIMIT or usr_int < MIN_AGE_LIMIT:
#         print("UEBAN! OUT!")
#     else:
#         if "7" in usr_age:
#             print('Вам сьогодні пощастить!')
#         elif 7 > usr_int < 16:
#             print('Де твої батьки?')
#         elif 7 < usr_int < 16:
#             print('Це фільм для дорослих!')
#         elif usr_int > 65:
#             print('Покажіть пенсійне посвідчення!')
#         else:
#             print('Ну ти й лох братішка, так підставитись!')
# print('\n(end)-----------------------------------------')
