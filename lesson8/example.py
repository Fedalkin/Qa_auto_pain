# Decorator
# Recursion


# def foo(funk):
#     print(id(funk))
#
#     def bar():
#         print('I\'m a function `bar`')
#
#     return bar
#
# res = foo(print)
#
# res()


# def my_decorator(funk_to_wrap):
#     def _wrapper(*args, **kwargs):
#         print('>>>>>Before function in my_decorator')
#         res = funk_to_wrap(*args, **kwargs)
#         print('<<<<<After function in my_decorator')
#         return res
#
#     return _wrapper
#
#
# def my_function(arg1, arg2):
#     print('I\'m just function')
#     print(args, kwargs)
#
#
# wrapped_function = my_decorator(my_function)
#
# wrapped_function()

# wrapped_print = my_decorator(print)
#
# wrapped_print('some data')

# my_function = my_decorator(my_function)
#
# my_function(1, 2)


def my_decorator(funk_to_wrap):
    def _wrapper(*args, **kwargs):
        print('>>>>>Before function in my_decorator')
        res = funk_to_wrap(*args, **kwargs)
        print('<<<<<After function in my_decorator')
        return res

    return _wrapper
#
#
# @my_decorator  # my_function = my_decorator(my_function)
# def my_function(arg1, arg2):
#     print('I\'m just function')
#     print(arg1, arg2)
#
#
# my_function('a', 'b')


# def only_int(funk_to_wrap):
#     def _wrapper(*args, **kwargs):
#         print('>>>>>Before function in my_decorator')
#
#         for arg in args:
#             if not isinstance(arg, int):
#                 raise TypeError(f'Arg should be int: {arg}')
#
#         for k_name, k_value in kwargs.items():
#             if not isinstance(k_value, int):
#                 raise TypeError(f'Kwarg {k_name} should be int: {k_value}')
#         res = funk_to_wrap(*args, **kwargs)
#         print('<<<<<After function in my_decorator')
#         return res
#
#     return _wrapper
#
#
# @only_int  # my_function = only_int(my_function)
# def my_function(arg1, arg2):
#     print('I\'m just function')
#     print(arg1, arg2)
#
#
# my_function(1, 1)
# my_function(1, 1.1)
# my_function(arg1=1, arg2=1)

def type_check(*types):
    def outer_wrapper(funk_to_wrap):
        def inner_wrapper(*args, **kwargs):

            for arg in args:
                if not isinstance(arg, types):
                    raise TypeError(f'Arg should be int: {arg}')

            for k_name, k_value in kwargs.items():
                if not isinstance(k_value, types):
                    raise TypeError(f'Kwarg {k_name} should be int: {k_value}')
            res = funk_to_wrap(*args, **kwargs)

            return res

        return inner_wrapper

    return outer_wrapper

@type_check(int, str)  # my_function = type_check(int, str)(my_function)
def my_function(arg1, arg2):
    print('I\'m just function')
    print(arg1, arg2)
#
#
# my_function(1, 1)
# # my_function(1, 1.1)
# my_function(arg1=1, arg2=1)
#
#
# @type_check(float)
# def my_function2(arg1, arg2):
#     print('I\'m just function')
#     print(arg1, arg2)
#
# my_function2(1.1, 1.2)


# def my_decorator_1(funk_to_wrap):
#     def _wrapper(*args, **kwargs):
#         print('>>>>>Before function in my_decorator_1')
#         res = funk_to_wrap(*args, **kwargs)
#         print('<<<<<After function in my_decorator_1')
#         return res
#
#     return _wrapper
#
#
# def my_decorator_2(funk_to_wrap):
#     def _wrapper(*args, **kwargs):
#         print('>>>>>Before function in my_decorator_2')
#         res = funk_to_wrap(*args, **kwargs)
#         print('<<<<<After function in my_decorator_2')
#         return res
#
#     return _wrapper
#
# @my_decorator_1
# @my_decorator_2
# def foo():  # foo = my_decorator_1(my_decorator_2(foo))
#     print('I\'m  function foo')

# foo()


# Imports

# import random
#
# print(random.random())

# import random as RANDOM
#
# print(RANDOM.random())

# from random import randint, choice
##f rom random import randint as R
## from random import choice as C

# print(R(1, 20))
# print(C([1, 20]))

# from random import * # randint, choice, .....
# print(randint(1, 20))
# print(choice([1, 20]))

# import library
#
# print(library.var2)
#
# library.foo()

# from library import var1, foo  # run library.py
#
# print(var1)
#
# foo()
#
# print(__name__)


#  Recursion

# !n
# !5 = 5 * 4 * 3 * 2 * 1
# !n = n * (n-1) * (n-2) ..... * 1

# f(5) - > 5 * f(4)

# counter = 0
#
# def factorial_r(n):
#     global counter
#     counter += 1
#
#     if n == 1:
#         return 1
#     else:
#         return n * factorial_r(n-1)
#
# print()
# res = factorial_r(100)
# print(res)
# print(counter)


# fibonacci

# 1 1 2 3 5 8 13 21 34 ....

# n[x] = n[x-1] + n[x-2]

counter = 0


def fib_rec(n):
    global counter
    counter += 1
    # O(n) = 2**n

    if n in (1, 2):
        return 1
    f1 = fib_rec(n - 1)
    f2 = fib_rec(n - 2)
    return f1 + f2


print(fib_rec(6))
print('counter', counter)


def fib_plain(n):
    fib1 = fib2 = 1
    if n in (1, 2):
        return fib2
    for _ in range(n - 2):
        # fib1, fib2 = fib2, fib1 + fib2

        _tmp_1 = fib2
        _tmp_2 = fib1 + fib2
        fib1 = _tmp_1
        fib2 = _tmp_2

    return fib2


res = fib_plain(6)
print(res)


n = int(input('Введите длину ряда: '))
f1 = f2 = 1
print(f1, f2, end=' ')

i = 2
while i < n:
    f1, f2 = f2, f1 + f2  # f1 приравнивается к f2, f2 приравнивается к f1 + f2
    print(f2, end=' ')  # Выводится f2
    i += 1
print()
