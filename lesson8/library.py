# var1 = 1
# var2 = 2
#
#
# def foo():
#     print('I\' function from library')
#
#
# if __name__ == '__main__':
#     print('some code from library')
#
#     print('some code from library')
#     foo()
#
#     print('--------> inside library:', __name__)

# -----------------------------------


# user_date = input('Enter a date if format "dd.mm": ')
def date_function(user_date):

    if user_date.endswith('01') or user_date.endswith('02') or user_date.endswith('12'):
        return 'Winter'
    elif user_date.endswith('03') or user_date.endswith('04') or user_date.endswith('05'):
        return 'Spring'
    elif user_date.endswith('06') or user_date.endswith('07') or user_date.endswith('08'):
        return 'Summer'
    elif user_date.endswith('09') or user_date.endswith('10') or user_date.endswith('11'):
        return 'Autumn'
    else:
        return None


# season = date_function(input('Enter a date if format "dd.mm": '))

# ----------------------------------------

def stupid_calc(number1: float, operator: str, number2: float):
    result = None
    if not isinstance(number1, (int, float)) or not isinstance(number2, (int, float)):
        print("Невірний тип даних")
        return result
    if operator == '+':
        result = number1 + number2
    elif operator == '-':
        result = number1 - number2
    elif operator == '/':
        result = number1 / number2
    elif operator == '*':
        result = number1 * number2
    else:
        print("Операція не підтримується")

    print('inside: ', result)
    return result

# stupid_calc(5, '+', 5)


def foo_work_time(foo_to_measure):
    def timer(*args, **kwargs):
        start = time.time()
        res = foo_to_measure(*args, **kwargs)
        end = time.time() - start
        print('Elapset time: ', end)
        return res

    return timer