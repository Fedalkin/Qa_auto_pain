# 1. Напишіть декоратор, який визначає час виконання функції. Заміряйте час іиконання функцій з попереднього ДЗ
import time


def foo_work_time(foo_to_measure):
    def timer(*args, **kwargs):
        start = time.time()
        res = foo_to_measure(*args, **kwargs)
        end = time.time() - start
        print('Elapset time: ', end)
        return res

    return timer

# from library import foo_work_time


# @foo_work_time  # date_function = foo_work_time(date_function)
# def date_function(user_date):
#     for _ in range(100000):
#         a = 10
#
#     if user_date.endswith('01') or user_date.endswith('02') or user_date.endswith('12'):
#         return 'Winter'
#     elif user_date.endswith('03') or user_date.endswith('04') or user_date.endswith('05'):
#         return 'Spring'
#     elif user_date.endswith('06') or user_date.endswith('07') or user_date.endswith('08'):
#         return 'Summer'
#     elif user_date.endswith('09') or user_date.endswith('10') or user_date.endswith('11'):
#         return 'Autumn'
#     else:
#         return None
# #
# #
# season = date_function(input('Enter a date if format "dd.mm": '))
# print(season)


# @foo_work_time
# def stupid_calc(number1: float, operator: str, number2: float):
#     result = None
#     if not isinstance(number1, (int, float)) or not isinstance(number2, (int, float)):
#         print("Невірний тип даних")
#         return result
#     if operator == '+':
#         result = number1 + number2
#     elif operator == '-':
#         result = number1 - number2
#     elif operator == '/':
#         result = number1 / number2
#     elif operator == '*':
#         result = number1 * number2
#     else:
#         print("Операція не підтримується")
#
#     print(result)
#     return result
#
# stupid_calc(5, '+', 5)

# 2. Візьміть функції з попереднього ДЗ, покладіть їх у файл lib.py і імпортуйте в основний файл для виконання

from library import date_function, stupid_calc

# date_function('02.04')
# print(date_function('02.04'))

res = stupid_calc(5, '+', 5)
print(res)
print(res)



