# Functions
# def function_name(arg1, arg2):
#     print("inside function_name arg1 is: ", arg1)
#     print("inside function_name arg2 is: ", arg2)
#     res = arg1 + arg2
#     return res
#
# result = function_name(1, 2)
# print('result is', result)
#------------------------------
# def custom_add(first, second):
#     print("inside function_name arg1 is: ", first)
#     print("inside function_name arg2 is: ", second)
#     res = first + second
#     return res
#
# result = custom_add(1, 2)
# print('result is', result)
#------------------------------

# def to_print(first): #bad idea
#     print(first)
# def custom_add(first, second):
#     print("inside function_name arg1 is: ", first)
#     print("inside function_name arg2 is: ", second)
#     res = first + second
#     return res
#-------------------------
# result = custom_add(5, 6)    # positional
# print(result)
#-----------------------
# result = custom_add(first=6, second=5)    # named
# print(result)
#-----------------------
# result = custom_add(
#     first=6,
#     second=5
# )    # named
# print(result)
#-------------------------
# def custom_function(a, b, c, d):
#     print(f'a is {a}')
#     print(f'a is {b}')
#     print(f'a is {c}')
#     print(f'a is {d}')

# custom_function(1, 2, 3, 4)
# custom_function(d=1, c=2, a=3, b=4)    # рекомендовано до використання
# custom_function(1, 2, c=10, d=20)   # спочатку позиційні, потім іменовані
#---------------------------
# def custom_function(a, b, c, d=0): # опціональний аргумент
#     print(f'a is {a}')
#     print(f'a is {b}')
#     print(f'a is {c}')
#     print(f'a is {d}')
#
# custom_function(1, 2, 3)
#----------------------------
# def custom_function(a=0, b=0, c=0, d=0):
#     if a == b == c == d == 0:
#         return
#     print(f'a is {a}')
#     print(f'a is {b}')
#     print(f'a is {c}')
#     print(f'a is {d}')
#
# custom_function(1, 2, 3)
#-------------------------------
# def custom_function(a=0, b=0, c=0, d=None):
#     if d is None:
#         return
#     print(f'a is {a}')
#     print(f'a is {b}')
#     print(f'a is {c}')
#     print(f'a is {d}')
#
# custom_function(d=4)
#-------------------------------
# def custom_function(a=0, b=0, c=0, d=None):
#     if d is None:
#         return
#     print(f'a is {a}')
#     print(f'a is {b}')
#
#     if a + b == 0:
#         return
#
#     print(f'a is {c}')
#     print(f'a is {d}')
#
# custom_function(c='asfg', d=4)
#--------------------------

# def add_to_list(value, list_to_add=[]):
#     list_to_add.append(value)
#     return list_to_add
#
# my_list = [1, 2, 3]
#
# my_list = add_to_list(4, my_list)
# print(my_list)
#
# my_list = add_to_list(5, my_list)
# print(my_list)
#
# my_new_list = add_to_list(1)
# print(my_new_list)
#
# my_list = add_to_list(6, my_list)
# print(my_list)
#
# my_new_list = add_to_list(1)
# print(my_new_list)

#todo - не треба використовувати мютебл типи данних для аргументів

# def add_to_list(value, list_to_add=None):
#
#     if list_to_add is None:
#         list_to_add = []
#
#     list_to_add.append(value)
#     return list_to_add


# def my_function(my_list):
#    my_list.append(1)
#    return my_list
#
# my_list = [1, 2, 3]
#
# my_list = my_function(my_list)
#
# print(my_list)


# def custom_function(a, b, c, d):
#     print(f'a is {a}, {type(a)}')
#     print(f'a is {b}, {type(b)}')
#     print(f'a is {c}, {type(c)}')
#     print(f'a is {d}, {type(d)}')
#     return
#
# custom_function(1, '2', True, None)

# def custom_function(*args): # * args - positional
#    print(f'a is {args}, {type(args)}')
#
#    return
#
# custom_function()
# custom_function(1, '2')
# custom_function(1, '2', True, None)
#
# my_list = [1, 2, 3, 4, 5]
# # custom_function(my_list[0], my_list[2])
# custom_function(*my_list)  # custom_function(my_list[0], my_list[2], ...)

# def custom_function(a, b, c, d, e):
#    print(a, b, c, d, e)
#
#    return
#
# custom_function(*my_list)

# my_list = [1, 2, 3, 4, 5]
# def custom_function(a, b=0, *args):
#    print(a)
#    print(b)
#    print(f'a is {args}, {type(args)}')
#    return
#
# custom_function(1, 2, 3, 4, 5, 5)
# custom_function(1, *my_list)

# my_list = [1, 2, 3, 4, 5]
# def custom_function(a, b, **kwargs): #  **kwargs - named
#
#    print(f'a is {kwargs}, {type(kwargs)}')
#    return
#
# custom_function(a=10, b=20, c=30)

# my_list = [1, 2, 3, 4, 5]
# my_dict = {
#    'm': 10,
#    'k': 30,
# }
# def custom_function(a, b, c=10, d=10, *args, **kwargs): #  **kwargs - named
#
#    print(a, b, c, d)
#    print(f'a is {args}, {type(args)}')
#    print(f'a is {kwargs}, {type(kwargs)}')
#    return
#
# custom_function(10, 20, 30, *my_list, **my_dict) # m=10, k=30

# my_dict = {
#    'a': 10,
#    'b': 10,
#    'c': 10,
#    'd': 10,
#    'm': 10,
#    'k': 30,
# }
# def custom_function(a, b, c=10, d=10, *args, **kwargs): #  **kwargs - named
#
#    print(a, b, c, d)
#    print(f'args is {args}, {type(args)}')
#    print(f'kwargs is {kwargs}, {type(kwargs)}')
#    return
#
# custom_function(**my_dict)

# GAME
   # AI  -> number
   # user get number from keyboard
   # user == AI

from random import randint

def get_ai_number():
   number = randint(1, 10)
   print(f'AI: {number}')
   return number

def get_user_number():
   while True:
      try:
         return int(input('Enter the number(int): '))
      except ValueError:
         print('Number please!')

def check_numbers(ai_number, user_number):
   result = ai_number == user_number
   print(f'Result is: {result}')
   return result

def game_guess_number():
   print('Game begins!')
   ai_number = get_ai_number()

   while True:
      user_number = get_user_number()
      is_game_end = check_numbers(ai_number, user_number)

      if is_game_end:
          break
      print('Wrong, try again!')

   print('User win')

game_guess_number()