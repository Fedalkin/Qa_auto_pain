# todo 1. Напишіть функцію, яка приймає два аргументи.
# a. Якщо обидва аргумени відносяться до числових типів функція пермножує ці аргументи і повертає результат
# b. Якшо обидва аргументи відносяться до типу стрінг функція обʼєднує їх в один і повертає
# c. В будь-якому іншому випадку - функція повертає кортеж з двох агрументів

# def my_function(arg1, arg2):
#     if type(arg1) in (int, float) and type(arg2) in (int, float):
#         result = arg1 * arg2
#         print(f'args is numbers. result: {result}')
#     elif str(arg1) == arg1 and str(arg2) == arg2:
#         result = str(arg1 + arg2)
#         print(f'args is str. result: {result}')
#     else:
#         result = (arg1, arg2)
#         print(f'args is shit. result: {result}')
#
#     return result
#
# res = my_function(2, 5)
# print(res)

# todo 2. Візьміть код з заняття і доопрацюйте натупним чином:
# a. користувач має вгадати чизло за певну кількість спроб. користувач на початку програми визначає кількість спроб
# b. додайте підказки. якщо рвзнися між числами більше 10 - "холодно", від 10 до 5 - "тепло", 1-4 - "гаряче"

from random import randint


def get_ai_number():
    number = randint(1, 26)
    print(f'AI: {number}')
    return number


def get_user_num_count():
    while True:
        try:
            return int(input('Enter the number of rounds(int): '))
        except ValueError:
            print('Number please!')


def get_user_number():
    while True:
        try:
            return int(input('Enter the number(int): '))
        except ValueError:
            print('Number please!')


def check_numbers(ai_number, user_number):
    result = ai_number == user_number
    dif = abs(ai_number - user_number)
    if dif > 10:
        print('COLD')
    elif dif >= 5:
        print('WARM')
    elif 0 < dif <= 4:
        print('HOT')

    return result


def game_guess_number():
    print('Game begins!')
    ai_number = get_ai_number()
    user_count = get_user_num_count()
    while True:

        if user_count < 1:
            print('YOU DIED')
            break
        user_number = get_user_number()
        is_game_end = check_numbers(ai_number, user_number)
        if is_game_end:
            print('User win')
            break
        else:
            print('Wrong, try again!')

        user_count -= 1

# var2
# def game_guess_number2():
#     print('Game begins!')
#     ai_number = get_ai_number()
#     user_count = get_user_num_count()
#
#     while user_count >= 1:
#         user_number = get_user_number()
#         is_game_end = check_numbers(ai_number, user_number)
#         if is_game_end:
#             print('User win')
#             break
#         else:
#             print('Wrong, try again!')
#         user_count -= 1
#     else:
#         print('YOU DIED')


game_guess_number()

