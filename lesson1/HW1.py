# Задача1: Створіть дві змінні first=10, second=30. Виведіть на екран результат математичної взаємодії (+, -, *, / и тд.) для цих чисел.
first = 10
second = 30
print('Test1: \n---------------------------------------------')
print('addition -', first + second)
print('subtraction -', first - second)
print('multiplication  -', first * second)
print('division -', first / second)
print('Modulus -', first % second)
print('Exponentiation -', first ** second)
print('Floor division	 -', first // second)
print('(end)---------------------------------------\n')
#
# Задача2: Створіть змінну і по черзі запишіть в неї результат порівняння (<, > , ==, !=) чисел з завдання 1. Виведіть на екран результат кожного порівняння.
print('Test2: \n--------------------------------------------')
comp_var = first < second
print('"Less than" -', comp_var)
comp_var = first > second
print('"Greater than" -', comp_var)
comp_var = first == second
print('"Equal" -', comp_var)
comp_var = first != second
print('"Not equal" -', comp_var)
comp_var = first >= second
print('"Greater than or equal to" -', comp_var)
comp_var = first <= second
print('"Less than or equal to" -', comp_var)
print('(end)---------------------------------------\n')
#
# Задача3: Створіть змінну - результат конкатенації строк "Hello " та "world!".
print('Test3: \n--------------------------------------------')
str1 = 'Hello '
str2 = 'world!'
konkat = str1 + str2
print(konkat)
print('\n(end)---------------------------------------')
