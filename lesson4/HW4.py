#1. todo/ Є list довільних чисел, наприклад [11, 77, 4, 22, 0, 56, 5, 95, 7, 5, 87, 13, 45, 67, 44].
# Напишіть код, який видалить (не створить новий, а саме видалить!) з нього всі числа, які менше 21 і більше 74.
# 1 var
# data_list = [11, 77, 4, 22, 0, 56, 5, 95, 7, 5, 87, 13, 45, 67, 44]
# print(data_list)
#
# copy_lsst = data_list.copy()
# for not_needed in copy_lsst:
#     if not_needed < 21 or not_needed > 74:
#         data_list[data_list.index(not_needed)] = None
#         print(data_list)
#
# while None in data_list:
#     data_list.remove(None)
# print(data_list)
# #
# 2 var
# data_list = [11, 77, 4, 22, 0, 56, 5, 95, 7, 5, 87, 13, 45, 67, 44]
# print(data_list)
#
# for not_needed in data_list:
#     if not_needed < 21 or not_needed > 74:
#         data_list[data_list.index(not_needed)] = None
#         # print(data_list)
#
# while None in data_list:
#     data_list.remove(None)
# print(data_list)
#
# 3 var
# #
# data_list = [11, 77, 4, 22, 0, 56, 5, 95, 7, 5, 87, 13, 45, 67, 44]
# print(data_list)
#
# for not_needed in data_list[:]:
#     if not_needed < 21 or not_needed > 74:
#         data_list.remove(not_needed)
#         print(data_list)
#
# print(data_list)


#2.todo/ Є два довільних числа які відповідають за мінімальну і максимальну ціну. Є Dict з назвами магазинів і цінами:
# { "citos": 47.999, "BB_studio" 42.999, "mo-no": 49.999, "my-main-service": 37.245, "buy-now": 38.324, "x-store": 37.166,
# "the-partner": 38.988, "store-123": 37.720, "roze-tka": 38.003}.
# Напишіть код, який знайде і виведе на екран назви магазинів, ціни яких потрапляють в діапазон між мінімальною і максимальною ціною.
# Наприклад:
#
# lower_limit = 35.9
#
# upper_limit = 37.339
#
# > match: "x-store", "main-service"

# 1 var===============
# my_dict = {"citos": 47.999, "BB_studio": 42.999, "mo-no": 49.999, "my-main-service": 37.245, "buy-now": 38.324, "x-store": 37.166, "the-partner": 38.988, "store-123": 37.720, "roze-tka": 38.003}
# lower_limit = 35.9
# upper_limit = 37.339
# my_list = []
# for key, value in my_dict.items():
#     if lower_limit > value or value < upper_limit:
#         my_list.append(key)
# print(f'match: {my_list}')

# 2 var===============
# print((key, value))
# my_dict = {"citos": 47.999, "BB_studio": 42.999, "mo-no": 49.999, "my-main-service": 37.245, "buy-now": 38.324, "x-store": 37.166, "the-partner": 38.988, "store-123": 37.720, "roze-tka": 38.003}
# lower_limit = 35.9
# upper_limit = 37.339
# result = ''
# for key, value in my_dict.items():
#     if lower_limit > value or value < upper_limit:
#         if not result:
#             prefix = 'match: '
#         else:
#             prefix = ', '
#         result = result + prefix + key
# print(result)

# 3 var=============
# my_dict = {"citos": 47.999, "BB_studio": 42.999, "mo-no": 49.999, "my-main-service": 37.245, "buy-now": 38.324, "x-store": 37.166, "the-partner": 38.988, "store-123": 37.720, "roze-tka": 38.003}
# lower_limit = 35.9
# upper_limit = 37.339
# my_list = []
# for key, value in my_dict.items():
#     if lower_limit > value or value < upper_limit:
#         my_list.append(key)
# print(f'match: {", ".join(my_list)}')

# 4 var===============
my_dict = {"citos": 47.999, "BB_studio": 42.999, "mo-no": 49.999, "my-main-service": 37.245, "buy-now": 38.324, "x-store": 37.166, "the-partner": 38.988, "store-123": 37.720, "roze-tka": 38.003}
lower_limit = 35.9
upper_limit = 37.339

my_list = [key for key, value in my_dict.items() if lower_limit > value or value < upper_limit]
res_str = ", ".join(my_list)
res = f'match: {res_str}'
print(res)

